#!/bin/bash

currentdir=`pwd`

# DON'T run with sudo.
if [ $USER == "root" ] 
then
	echo "Do not run as root or with sudo."
	exit
fi

# Confirmation
read -r -p "Are you sure? [y/N] " response
response=${response,,} # lower
if [ $response == "y" ]
then
	echo "Installing..."
else
	echo "Stopping."
	exit
fi

# Backup
echo ""
echo "Moving old config files"
prefix="old-"
config_files=(".vimrc"
		".tmux.conf")
cd ~
for file in ${config_files[*]} 
do
	if [ -e "$file" ]
	then
		echo "moving \"$file\""
		mv "$file" "$prefix$file"
	else
		echo "\"$file\" not found, skipping."
	fi
done

# Apt
echo ""
echo "Installing apt packages"
sudo apt-get install python2.7 python-dev python3 python3-dev fish vim ssh tmux git iverilog

# SSH
# Confirmation
read -r -p "Do you want to make a new ssh key? [y/N] " response
response=${response,,} # lower
if [ $response == "y" ]
then
	echo ""
	echo "Making new ssh key"
	ssh-keygen -t ed25519
else
	echo "Skipping SSH key"
fi

# Git
echo ""
echo "Pulling git repos"
git clone https://gitlab.com/CaptainGector/configs.git ~/configs # Config files
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim # Vundle

# Vim
echo ""
echo "Installing vim plugins."
echo | vim +PluginInstall +qall # Sets up plugin manager N stuff

# Compiling YCM
echo ""
echo "Compiling YCM"
cd ~/.vim/bundle/YouCompleteMe
./install.py
cd $currentdir

# Setup fish
echo ""
echo "Setting up fish."
fish_dir=`which fish`
echo "chsh: "
chsh --shell $fish_dir $USER

# Link everything up
echo ""
echo "Linking config files."
ln -sf ~/configs/vimrc-common ~/.vimrc
echo "Which tmux config do you want to use?"
ls tmux*
read FILE
if test -f "$FILE"; then
    echo "$FILE exists"
    ln -sf ~/configs/$FILE ~/.tmux.conf
fi

# Test colors
cd $currentdir
./test_colors.sh

# DONE!
echo "--------------------------------- Done installing ----------------------------"
echo "Tmux has been setup with a server configuration, you can change this in ~/.tmux.conf"
echo "Old config files have been renamed with a 'old-' prefix."
